/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:36:46 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_str_is_alpha(char *str)
{
	int		allchar;
	int		in;

	in = 0;
	allchar = 1;
	while (str[in] != '\0')
	{
		if (str[in] < 65 || str[in] > 122 || (str[in] >= 91 && str[in] <= 96))
		{
			allchar = 0;
		}
		in++;
	}
	return (allchar);
}

/*int	main(void)
{
	char	returned;

	returned = ft_str_is_alpha("asds") + 48;
	write(1, &returned, 1);
}*/

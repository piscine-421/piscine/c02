/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:36:52 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_str_is_uppercase(char *str)
{
	int		all_uppercase;
	int		index;

	index = 0;
	all_uppercase = 1;
	while (str[index] != '\0')
	{
		if (str[index] < 65 || str[index] > 90)
		{
			all_uppercase = 0;
		}
		index++;
	}
	return (all_uppercase);
}

/*int	main(void)
{
	char	returned;

	returned = ft_str_is_uppercase("ASFAFDS") + 48;
	write(1, &returned, 1);
}*/

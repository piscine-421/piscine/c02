/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:37:00 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_str_is_printable(char *str)
{
	int		all_printable;
	int		index;

	index = 0;
	all_printable = 1;
	while (str[index] != '\0')
	{
		if (str[index] < 32 || str[index] > 126)
		{
			all_printable = 0;
		}
		index++;
	}
	return (all_printable);
}

/*int	main(void)
{
	char	returned;

	returned = ft_str_is_printable("ASFAF aDS") + 48;
	write(1, &returned, 1);
}*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:37:21 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr_non_printable(char *str)
{
	int		index;
	char	hex[2];

	index = 0;
	while (str[index] != '\0')
	{
		if (str[index] < 32 || str[index] > 126)
		{
			write(1, "\\", 1);
			if (str[index] / 16 > 9)
				hex[0] = str[index] / 16 + 87;
			else
				hex[0] = str[index] / 16 + 48;
			if (str[index] % 16 > 9)
				hex[1] = str[index] % 16 + 87;
			else
				hex[1] = str[index] % 16 + 48;
			write(1, &hex, 2);
		}
		else
			write(1, &str[index], 1);
		index++;
	}
}

/*int	main(void)
{
	ft_putstr_non_printable("Coucou\etu vas bien ?");
}*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:36:43 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	int		index;

	index = 0;
	while (n > 0 && src[index] != '\0')
	{
		dest[index] = src[index];
		index++;
		n--;
	}
	while (n > 0)
	{
		dest[index] = '\0';
		index++;
		n--;
	}
	return (dest);
}

/*int	main(void)
{
	char	string[] = "something";
	char	string2[] = "        ";

	ft_strncpy(string2, string, 9);
	write(1, &string2, 9);
}*/

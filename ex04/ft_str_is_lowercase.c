/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:36:50 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_str_is_lowercase(char *str)
{
	int		all_lowercase;
	int		index;

	index = 0;
	all_lowercase = 1;
	while (str[index] != '\0')
	{
		if (str[index] < 97 || str[index] > 122)
		{
			all_lowercase = 0;
		}
		index++;
	}
	return (all_lowercase);
}

/*int	main(void)
{
	char	returned;

	returned = ft_str_is_lowercase("asds") + 48;
	write(1, &returned, 1);
}*/

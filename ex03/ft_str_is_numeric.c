/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:36:48 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

int	ft_str_is_numeric(char *str)
{
	int		allnum;
	int		index;

	index = 0;
	allnum = 1;
	while (str[index] != '\0')
	{
		if (str[index] < 48 || str[index] > 57)
		{
			allnum = 0;
		}
		index++;
	}
	return (allnum);
}

/*int	main(void)
{
	char	returned;

	returned = ft_str_is_numeric("4564656") + 48;
	write(1, &returned, 1);
}*/

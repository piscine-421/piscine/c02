/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:37:35 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb)
{
	char		nbc;
	int			divisor;
	int			start;
	long int	nb2;

	nb2 = nb;
	divisor = 1000000000;
	if (nb2 < 0)
	{
		write (1, "-", 1);
		nb2 *= -1;
	}
	while (divisor >= 1)
	{
		nbc = nb2 / divisor % 10 + 48;
		divisor /= 10;
		if (nbc != '0')
		{
			start = 1;
		}
		if (start == 1)
		{
			write (1, &nbc, 1);
		}
	}
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	size = 0;
	return (&addr);
}

/*int	main(void)
{
	char	big_damn_string[] = "Bonjour les aminches\t\n\tc\a est fou\ttout\tce qu on peut faire avec\t\n\tprint_memory\n\n\n\tlol.lol\n \0";
	int		nb;

	nb = (int)ft_print_memory(big_damn_string, 92);
}*/

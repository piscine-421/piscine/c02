/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/19 18:37:17 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	int				index;
	unsigned int	size2;

	index = 0;
	size2 = size;
	while (src[index] != '\0' && size2 > 1)
	{
		dest[index] = src[index];
		index++;
		size2--;
	}
	while (size2 > 1)
	{
		dest[index] = '\0';
		index++;
		size2--;
	}
	dest[index] = '\0';
	return (size);
}

/*int	main(void)
{
	char	string[9] = "aomething";
	char	string2[] = "        ";

	ft_strlcpy(string2, string, 4);
	write(1, &string2, 9);
}*/
